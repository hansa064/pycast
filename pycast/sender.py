#!/usr/bin/env python
# -*- python -*- coding: utf-8 -*-

import socket
import struct

try:
    import cPickle as pickle
except ImportError:
    import pickle


class MulticastSender(object):
    """
    Send Multicast messages.
    This object sends messages to the multicast group listening on (group, port).
    As UDP doesn't ensure the order of the packages, this class tries to enable the recipients to be able to restore the sequence of packages.

    Example usage:

    sender = MulticastSender(MCAST_GROUP, MCAST_PORT)

    msg = "This is a simple test message"
    sender.send(msg)
    sender.close()
    """
    def __init__(self, group, port):
        """
        Create a new sender
        :param group: The IPv4 Multicast group to send the messages to
        :param port: The IPv4 Multicast port to send messages to
        :return: A new multicast sender which is able to send messages to the multicast group (group, port).
        """
        self.__addr = (group, port)
        # Create an UDP socket
        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self.__socket.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)
        self.__msg_id = 0

    def send(self, msg):
        """
        Send a multicast message.
        This method sends a message to all recipients listening on the multicast group.

        :param msg: The message to send to the recipients.
        :return: The amount of bytes send.
        :raise socket.error: If a network error occurred, this exception will be raised.
        """
        msg = str(msg)
        msg = struct.pack("I", self.__msg_id) + msg
        self.__msg_id += 1
        return self.__socket.sendto(msg, self.__addr)

    def close(self):
        """
        Close the socket of this object.
        Once this method is called, this object is not usable anymore.
        """
        self.__socket.close()

    def __del__(self):
        self.close()


class MulticastObjectSender(MulticastSender):
    """
    Send Multicast objects.
    This object sends serializable objects to the multicast group listening on (group, port).
    As UDP doesn't ensure the order of the packages, this class tries to enable the recipients to be able to restore the sequence of packages.

    Example usage:

    sender = MulticastObjectSender(MCAST_GROUP, MCAST_PORT)

    obj = object()
    sender.send(obj)
    sender.close()
    """
    def send(self, obj):
        """
        Send a multicast object.
        This method sends a message to all recipients listening on the multicast group.

        :param obj: The object to send to the recipients.
        :return: The amount of bytes send.
        :raise socket.error: If a network error occurred, this exception will be raised.
        """
        obj_str = pickle.dumps(obj)
        size = struct.pack("I", len(obj_str))
        # First send the size
        super(MulticastObjectSender, self).send(size)
        # Then send the object
        super(MulticastObjectSender, self).send(obj_str)
