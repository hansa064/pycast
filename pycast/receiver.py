#!/usr/bin/env python
# -*- python -*- coding: utf-8 -*-

import socket
import struct

try:
    import cPickle as pickle
except ImportError:
    import pickle


class PackageSequenceError(Exception):
    """
    This exception is raised when the sequence of the packages got lost during transmission.
    The fact that this exception is raised may not be difficult at all, as the sequence may be restored.
    """

    def __init__(self, expected_id, actual_id):
        super(PackageSequenceError, self).__init__("Expected ID %d but got %d" % (expected_id, actual_id))
        self.expected_id = expected_id
        self.actual_id = actual_id


class MulticastReceiver(object):
    """
    Receive Multicast messages.
    This object listens on (group, port) and and is able to receive messages send on this.
    As UDP doesn't ensure the order of the packages, this class tries to restore the original order of the packages.

    Example usage:

    receiver = MulticastReceiver(MCAST_GROUP, MCAST_PORT)
    try_again = True
    while True:
        try:
            msg = receiver.receive()
            try_again = True
        except PackageSequenceError as e:
            print e
            if not try_again:
                receiver.skip_package()
            try_again = False
    """

    def __init__(self, group, port, interface="", timeout=None):
        """
        Create a new receiver
        :param group: The IPv4 Multicast group to join
        :param port: The IPv4 Multicast port to listen on for messages
        :param interface: Bind to a specific interface (default: Bind to all interfaces)
        :param timeout: Set a timeout for the socket to receive a message (default: No timeout, blocking receive)
        :return: A new multicast receiver which listens on interface and has joined the (group, port) multicast group
        """
        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self.__socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.__socket.bind((interface, port))
        mreq = struct.pack("4sl", socket.inet_aton(group), socket.INADDR_ANY)

        self.__socket.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
        # Set the socket to `(non)blocking`
        self.__socket.settimeout(timeout)

        self.__msg_id = None
        self.__messages = {}

    def receive(self, length=1024):
        """
        Receive `length` bytes from the socket.
        This method tries to receive exactly `length` bytes from the socket. If the Datagram is smaller or greater to the given
        length an `socket.error` will be raised.
        If `timeout` is set on the receiver, this function may raise a `socket.timeout` if no data have been received after the given timeout.

        :param length: The length of the datagram to receive from the socket.

        :return: The received message

        :raise PackageSequenceError: If the order of the packages is wrong, this exception will be raised to signal that a package may be lost.
        :raise socket.error: If the size of the datagram differs from the requestet `length` or another network error occurred, this exception will be raised.
        :raise socket.timeout: If `timeout` is set on the receiver and no data are received within the timeout, this exception will be raised.
        """
        # Check if package is in the buffer
        if self.__msg_id in self.__messages.keys():
            return self.__messages.pop(self.__msg_id)

        # If not, try to receive it
        int_size = struct.calcsize("I")
        length += int_size
        msg = self.__socket.recv(length)
        msg_id = struct.unpack("I", msg[:int_size])[0]
        if self.__msg_id is None:
            self.__msg_id = msg_id
        if msg_id != self.__msg_id:
            # It's not the correct DGRAM, buffer it..
            self.__messages[msg_id] = msg[int_size:]
            # Raise an error to signal that the order of the packages is wrong
            raise PackageSequenceError(self.__msg_id, msg_id)
        else:
            # It's the correct DGRAM, return..
            self.__msg_id += 1
            return msg[int_size:]

    def close(self):
        """
        Close the socket and stop listening for multicast packages.
        After calling this method once, this object is not usable anymore.

        :raise socket.error: If a network error occurs, a socket.error exception will be raised.
        """
        self.__socket.close()

    def skip_package(self):
        """
        Skip the next package.
        This method may be called if a package got lost and the system wants to skip it to continue with the operation.
        """
        self.__msg_id += 1

    def __del__(self):
        self.close()


class MulticastObjectReceiver(MulticastReceiver):
    """
    Receive Multicast objects.
    This object listens on (group, port) and and is able to receive messages send on this.
    As UDP doesn't ensure the order of the packages, this class tries to restore the original order of the packages.
    This is a specialization of the `MulticastReceiver` to ensure the transmission of any serializable object.


    Example usage:

    receiver = MulticastObjectReceiver(MCAST_GROUP, MCAST_PORT)
    try_again = True
    while True:
        try:
            msg = receiver.receive()
            try_again = True
        except PackageSequenceError as e:
            print e
            if not try_again:
                receiver.skip_package()
            try_again = False
    """

    def __init__(self, group, port, interface="", timeout=None):
        """
        Create a new receiver
        :param group: The IPv4 Multicast group to join
        :param port: The IPv4 Multicast port to listen on for messages
        :param interface: Bind to a specific interface (default: Bind to all interfaces)
        :param timeout: Set a timeout for the socket to receive a message (default: No timeout, blocking receive)
        :return: A new multicast receiver which listens on interface and has joined the (group, port) multicast group
        """
        super(MulticastObjectReceiver, self).__init__(group=group, port=port, interface=interface, timeout=timeout)
        self.__message = None

    def receive(self, length=None):
        """
        Receive `length` bytes from the socket.
        This method tries to receive exactly `length` bytes from the socket. If the Datagram is smaller or greater to the given
        length an `socket.error` will be raised.
        If `timeout` is set on the receiver, this function may raise a `socket.timeout` if no data have been received after the given timeout.

        :param length: The length of the datagram to receive from the socket.

        :return: The received message

        :raise PackageSequenceError: If the order of the packages is wrong, this exception will be raised to signal that a package may be lost.
        :raise socket.error: If the size of the datagram differs from the requested `length` or another network error occurred, this exception will be raised.
        :raise socket.timeout: If `timeout` is set on the receiver and no data are received within the timeout, this exception will be raised.
        """
        if length is None:
            length = self.receive(length=struct.calcsize("I"))
            length = struct.unpack("I", length)[0]
            obj_str = self.receive(length=length)
            return pickle.loads(obj_str)
        else:
            return super(MulticastObjectReceiver, self).receive(length)
