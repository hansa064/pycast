#!/usr/bin/env python
# -*- python -*- coding: utf-8 -*-

import unittest
from pycast.receiver import MulticastObjectReceiver
from pycast.sender import MulticastObjectSender


MCAST_GRP = "224.1.1.1"
MCAST_PORT = 5007


class TestObjClass(object):
    def __init__(self, id):
        self.id = id

    def __eq__(self, other):
        return self.id == other.id


class SendAndReceive(unittest.TestCase):
    def setUp(self):
        self.__sender = MulticastObjectSender(MCAST_GRP, MCAST_PORT)
        self.__receiver = MulticastObjectReceiver(MCAST_GRP, MCAST_PORT)

    def tearDown(self):
        self.__sender.close()
        self.__receiver.close()

    def sendAndReceiveObject(self, obj):
        self.__sender.send(obj)
        check_obj = self.__receiver.receive()
        self.assertEqual(check_obj, obj)

    def testSendAndReceiveString(self):
        self.sendAndReceiveObject("Hallo Welt!")

    def testSendAndReceiveObject(self):
        obj = TestObjClass(id=1337)
        self.sendAndReceiveObject(obj)

    def testSendAndReceiveLargeString(self):
        test_message = "H" * 10000
        self.sendAndReceiveObject(test_message)


if __name__ == '__main__':
    unittest.main()
