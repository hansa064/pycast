#!/usr/bin/env python
# -*- python -*- coding: utf-8 -*-

import socket
import unittest
from pycast.receiver import MulticastReceiver
from pycast.sender import MulticastSender


MCAST_GRP = "224.1.1.1"
MCAST_PORT = 5007


class SendAndReceive(unittest.TestCase):
    def setUp(self):
        self.__sender = MulticastSender(MCAST_GRP, MCAST_PORT)
        self.__receiver = MulticastReceiver(MCAST_GRP, MCAST_PORT)

    def tearDown(self):
        self.__sender.close()
        self.__receiver.close()

    def testSendAndReceive(self):
        test_message = "Hallo Welt"
        self.__sender.send(test_message)
        check_msg = self.__receiver.receive(len(test_message))
        self.assertEqual(check_msg, test_message)

    def testSendAndReceiveNonBlocking(self):
        self.__receiver = MulticastReceiver(MCAST_GRP, MCAST_PORT, timeout=0.2)
        try:
            self.__receiver.receive()
        except Exception as e:
            self.assertIsInstance(e, socket.timeout)
        self.testSendAndReceive()

    def testSendAndReceiveLargeString(self):
        test_message = "H" * 10000
        self.__sender.send(test_message)
        check_msg = self.__receiver.receive(len(test_message))
        self.assertEqual(check_msg, test_message)


if __name__ == '__main__':
    unittest.main()
