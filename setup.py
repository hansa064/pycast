#!/usr/bin/env python
# -*- python -*- coding: utf-8 -*-

from setuptools import setup

setup(
    name="PyCast",
    version="0.1",
    description="A simple python library for multicast support.",
    long_description="A small and simple library for multicast applications with python. This lib includes support for transmitting ordered messages and pickable objects.",
    author="Torben Hansing",
    author_email="hansa064@hotmail.com",
    url="https://bitbucket.org/hansa064/pycast",
    packages=["pycast"],
    license="LGPLv3",
    keywords="python multicast hansing library",
    zip_safe=True,
    test_suite="tests",
)
